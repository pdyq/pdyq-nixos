{ pkgs }:
pkgs.stdenv.mkDerivation {
  name = "sddm-theme";
  # src = pkgs.fetchFromGitHub {
  #   "owner" = "pdyq";
  #   "repo" = "sddm-sugar-dark";
  #   "rev" = "ceb2c455663429be03ba62d9f898c571650ef7fe";
  #   "sha256" = "0153z1kylbhc9d12nxy9vpn0spxgrhgy36wy37pk6ysq7akaqlvy";
  # };
  src = /home/pdyq/pdyq-nixos/src/sddm-theme;
  installPhase = ''
    mkdir -p $out
    cp -R ./* $out/
  '';
}