# Rime user dictionary
#@/db_name	japanese
#@/db_type	userdb
#@/rime_version	1.8.5
#@/tick	12
#@/user_id	6eec6033-44c8-4e03-b91b-17ca32b12a97
daiki 	大樹	c=1 d=0.995012 t=12
jin 	人	c=1 d=1 t=12
oishii ne 	美味しいね	c=1 d=1 t=12
se 	せ	c=1 d=0.99005 t=12
shin 	新	c=1 d=1 t=12
taiwan 	台湾	c=1 d=0.995012 t=12
tenki 	天気	c=1 d=1 t=12
wa 	わ	c=3 d=2.94573 t=12
watashi 	私	c=2 d=1.97025 t=12
