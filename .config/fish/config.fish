if status is-interactive
    # Commands to run in interactive sessions can go here
end

function fish_greeting
    echo 
    neofetch
end

function fish_prompt
    echo
    echo $(set_color blue) $(prompt_pwd) $(set_color green)">  "
end

alias ls="eza -a --icons"
alias ll="eza -lah --icons"
alias src="source ~/.config/fish/config.fish"
alias soy="doas systemctl"
alias cat="bat"

set GTK_IM_MODULE fcitx
set QT_IM_MODULE fcitx
set XMODIFIERS "@im=fcitx"
