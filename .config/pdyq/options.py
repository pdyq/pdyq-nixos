#!/usr/bin/env python3
flake = True
flake_location = "/home/pdyq/pdyq-nixos/"
flake_username = "pdyq"
flake_hostname = "pdyq-laptop"
config_location = "/home/pdyq/pdyq-nixos/nixos/configuration.nix"
hm_config_location = "/home/pdyq/pdyq-nixos/home-manager/home.nix"
hm_file_location = "/home/pdyq/pdyq-nixos/home-manager/apps"
editor = "hx"
# sudo = True
# sudo_command = "sudo"
impure = True
switch = True
test = False
boot = False
template = True
extraCommands = ""
hm_extraCommands = ""