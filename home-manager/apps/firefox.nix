{ pkgs, ... }:
{
  home.packages = [ pkgs.firefox pkgs.tridactyl-native ];
}
