{ pkgs, ... }:
{
  home.packages = [ pkgs.dwmblocks ];
  nixpkgs.overlays = [ (final: prev: {
    dwmblocks = prev.dwmblocks.overrideAttrs {src = /home/pdyq/pdyq-nixos/src/dwmblocks;};
  }
  )];
}