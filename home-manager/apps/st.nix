{ pkgs, ... }:
{
  home.packages = [ pkgs.st ];
  nixpkgs.overlays = [ (final: prev: {
    st = prev.st.overrideAttrs {src = /home/pdyq/pdyq-nixos/src/st;};
  }
  )];
}