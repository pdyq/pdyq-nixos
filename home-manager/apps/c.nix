{ pkgs, ... }:
{
  home.packages = with pkgs; [ 
    gcc
    llvm
    clang-tools
  ];
}
