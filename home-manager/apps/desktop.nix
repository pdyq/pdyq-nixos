{ pkgs, ... }:
{
  home.packages = [ 
    pkgs.waybar
    pkgs.hyprpaper
    pkgs.bemenu
    pkgs.brightnessctl
    pkgs.kbdlight
    pkgs.picom
    pkgs.feh
  ];

  home.file.".config/hypr" = {
    source = /home/pdyq/pdyq-nixos/.config/hypr;
    recursive = true;
  };
  home.file.".config/waybar" = {
    source = /home/pdyq/pdyq-nixos/.config/waybar;
    recursive = true;
  };
  home.file.".local/share/dwm/autostart.sh".source = /home/pdyq/pdyq-nixos/src/dwm/autostart.sh;
}
