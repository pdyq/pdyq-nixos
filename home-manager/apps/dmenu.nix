{ pkgs, ... }:
{
  home.packages = [ pkgs.dmenu ];
  nixpkgs.overlays = [ (final: prev: {
    dmenu = prev.dmenu.overrideAttrs {src = /home/pdyq/pdyq-nixos/src/dmenu;};
  }
  )];
}