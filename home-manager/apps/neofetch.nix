{ pkgs, ... }:
{
  home.packages = [ pkgs.neofetch ];
  home.file.".config/neofetch".source = /home/pdyq/pdyq-nixos/.config/neofetch;
}

