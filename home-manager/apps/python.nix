{ pkgs, ... }:
{
  home.packages = [ 
      (pkgs.python3.withPackages(
        ps: with ps; [
          typer
          rich
          python-lsp-server
        ]
      ))
  ];
}
