{ pkgs, ... }:
{
  home.packages = [ pkgs.kitty];
  home.file.".config/kitty" = {
    source = /home/pdyq/pdyq-nixos/.config/kitty;
    recursive = true;
  };
}
