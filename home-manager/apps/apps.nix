{ pkgs, ... }:
{
  home.packages = with pkgs; [ 
    wget
    eza
    htop
    btop
    pavucontrol
    tldr
    killall
    bitwarden
    gtklock
    gimp
    mpv
    sxiv
    irssi
    # obs-studio
    # mdbook
    # audacity
    # libreoffice-fresh
    bat
    flameshot
    gnumake
    bc
    feh
   ];
}

