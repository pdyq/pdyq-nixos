{ pkgs, ... }:
{
  home.packages = [ 
    pkgs.helix
    pkgs.nodePackages.vscode-html-languageserver-bin
    pkgs.nil
  ];
  home.file.".config/helix/config.toml".source = /home/pdyq/pdyq-nixos/.config/helix/config.toml;
}
