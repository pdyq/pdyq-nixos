{ pkgs, ... }:
{
  home.packages = [ 
    pkgs.fish
 ];
  programs.fish = {
    enable = true;
  };
  home.file.".config/fish" = {
    source = ~/pdyq-nixos/.config/fish;
    recursive = true;
  };
}
