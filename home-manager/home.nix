{ config, pkgs, ... }:
{
  imports = map (n: "${./apps}/${n}") (builtins.attrNames (builtins.readDir ./apps));

  # Self-introduction
  home.username = "pdyq";
  home.homeDirectory = "/home/pdyq";

  # GTK
  gtk.enable = true;
  gtk.theme = {
    name="Catppuccin-Mocha-Compact-Pink-dark";
    package = pkgs.catppuccin-gtk.override {
        accents = [ "pink" ];
        size = "compact";
        tweaks = [ "normal" ];
        variant = "mocha";
    };
  };

  gtk.cursorTheme = {
    name = "Catppuccin-Mocha-Dark-Cursors";
    package = pkgs.catppuccin-cursors.mochaDark;
    size = 24;
  };

  gtk.iconTheme = {
    name = "Papirus Dark";
    package = pkgs.papirus-icon-theme;
  };
  # gtk.theme.package = pkgs.dracula-theme;
  # gtk.theme.name = "Dracula";

  # QT
  qt.enable = true;
  qt.platformTheme = "gtk";  

  # Version
  home.stateVersion = "23.05";

  # Unfree packages
  nixpkgs.config.allowUnfree = true;
}
