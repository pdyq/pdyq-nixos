#!/usr/bin/env bash
xrandr -s 1680x1050
picom &
fcitx5 &
dwmblocks &
feh --bg-fill /home/pdyq/pdyq-nixos/wallpapers/nixppuccin.png &
